<?php
namespace Xaben\PageBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BreadcrumbAdmin extends AbstractAdmin
{
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('slug')
            ->add('data');
    }

    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('slug', 'text', array('required' => true))
            ->add('data', 'textarea', array(
                    'required' => true,
                    'attr' => array(
                        //'class' => 'tinymce',
                        'style' => 'height:100px; width: 100%'
                    )
                )
            );
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('slug');
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('slug');
    }
}
