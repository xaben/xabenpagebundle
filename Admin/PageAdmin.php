<?php
namespace Xaben\PageBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PageAdmin extends AbstractAdmin
{
    /**
     * @var array
     */
    private $config;

    /**
     * PageAdmin constructor.
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param array $config
     */
    public function __construct($code, $class, $baseControllerName, $config)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->config = $config;
    }

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('slug')
            ->add('title')
            ->add('description')
            ->add('keywords')
        ;

        if ($this->config['localized']) {
            $showMapper->add('locale');
        }
    }

    public function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('Page',
                array(
                    'class'       => 'col-md-8',
                ));

        if ($this->config['localized']) {
            $formMapper
                ->add('locale', 'choice', array('choices' => $this->getConfiguredLocales()));
        }

        $formMapper
                ->add('slug', null, array('required' => false))
                ->add('title', null, array('required' => true))
                ->add('content', CKEditorType::class, array('required' => true))
                ->add('breadcrumbs', 'sonata_type_collection', array('required'=>false, 'by_reference' => false), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'targetEntity' => 'Xaben\TopBundle\Entity\Options'
                ))
            ->end()
            ->with('SEO',
                array(
                    'class'       => 'col-md-4',
                ))
                ->add('description', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
            ->end()
        ;
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('slug')
            ->add('title')
        ;

        if ($this->config['localized']) {
            $listMapper->add('locale', 'choice', array('choices' => $this->getConfiguredLocales()));
        }
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('slug')
            ->add('title')
        ;
    }

    private function getConfiguredLocales()
    {
        if (!$this->config['localized']) {
            return [];
        }

        return array_combine(array_values($this->config['locales']), array_values($this->config['locales']));
    }
}
