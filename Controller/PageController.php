<?php

namespace Xaben\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function defaultAction($slug, $_locale)
    {
        $config = $this->getParameter('xaben_page');
        $pageRepository = $this->getDoctrine()->getRepository('XabenPageBundle:Page');

        if ($config['localized']) {
            $page = $pageRepository->getPage($slug, $_locale);
        }

        if (empty($page)) {
            $page = $pageRepository->getPage($slug);
        }

        return $this->render ( $config['default_template'], array ('slug' => $slug, 'page'=>$page, 'showtitle'=>true, 'layout'=> $config['layout_template']) );
    }
}
