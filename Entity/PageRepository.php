<?php

namespace Xaben\PageBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PageRepository extends EntityRepository
{
    public function getPage($slug, $locale = null)
    {
        /** @var QueryBuilder $qb */
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p, b')
            ->leftJoin('p.breadcrumbs', 'b')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ;

        if ($locale) {
            $qb
                ->andWhere('p.locale = :locale')
                ->setParameter('locale', $locale);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
