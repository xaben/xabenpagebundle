<?php
namespace Xaben\PageBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapListener implements SitemapListenerInterface
{
    private $router;
    private $em;

    public function __construct(RouterInterface $router, EntityManager $em)
    {
        $this->router = $router;
        $this->em = $em;
    }

    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $section = $event->getSection();
        if (is_null($section) || $section == 'page') {

            //get list of all pages
            $pages = $this->em->getRepository('XabenPageBundle:Page')->findAll();

            foreach ($pages as $page) {
                //get absolute url
                $url = $this->router->generate('xaben_page_homepage', array('slug'=>$page->getSlug()), true);

                //add homepage url to the urlset named default
                $event->getGenerator()->addUrl(
                    new UrlConcrete(
                        $url,
                        $page->getUpdated(),
                        UrlConcrete::CHANGEFREQ_WEEKLY,
                        1
                    ),
                    'pages'
                );
            }
        }
    }
}
