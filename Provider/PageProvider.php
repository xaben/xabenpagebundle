<?php

namespace Xaben\PageBundle\Provider;

use Doctrine\ORM\EntityManager;

class PageProvider
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $slug
     */
    public function get($slug)
    {
        return $this->em->getRepository('XabenPageBundle:Page')->getPage($slug);
    }
}
