# XabenPageBundle #

The bundle provides simple CMS functionality including breadcrumb editing for your templates.

### Instalation ###

Download XabenForumBundle and its dependencies to the ``vendor`` directory. You
can use Composer for the automated process:

```console
php composer.phar require xaben/page-bundle --no-update
php composer.phar update
```

Next, be sure to enable the bundle in your AppKernel.php file:

```php
// app/AppKernel.php
public function registerBundles()
{
    return array(
        // ...
        new Xaben\PageBundle\XabenPageBundle(),
        // ...
    );
}
```

Now, install the assets from the bundle:

```console
php app/console assets:install web
```

Import the forum routes into your routing file:

```yaml
# app/config/routing.yml
xaben_page:
    resource: "@XabenPageBundle/Resources/config/routing.yml"
    prefix:   /page
```

Usually when installing new bundles it's good practice to also delete your cache:

```console
php app/console cache:clear
```

Enable assetic for the forum bunlde:
```yaml
assetic:
    # ....
    bundles:        [ XabenPageBundle ]
```

Now create or update database schema:

```console
php app/console doctrine:schema:update
```

### Usage ###

In your controller get the page object and pass it to the template:

```php
        ...
        $page = $this->get('xaben.page.provider')->get('servicii');
        ...
```

Inside the template :

* to display the page:
```twig
{{ pageObject.content|raw }}
```

* to display a breadcrumb do:
```twig
{{ page_breadcrumb( pageOject, breadcrumbSlug )  }}
```