<?php

namespace Xaben\PageBundle\Tests\DependencyInjection;

use Xaben\PageBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;

class ConfigurationTest extends \PHPUnit_Framework_TestCase
{
    public function testPageEmptyConfiguration()
    {
        $processor = new Processor();

        $config = $processor->processConfiguration(new Configuration(), array(array(
        )));

        $expected = array(
            'default_template' => 'XabenPageBundle:Page:default.html.twig',
            'layout_template' => 'XabenSiteBundle::layout.html.twig',
        );

        $this->assertEquals($expected, $config);
    }
}