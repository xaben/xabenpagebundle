<?php

namespace Xaben\PageBundle\Tests\Twig\Extension;

use Xaben\PageBundle\Entity\Breadcrumb;
use Xaben\PageBundle\Entity\Page;
use Xaben\PageBundle\Twig\Extension\PageExtension;

class PageExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testBreadcrumbs(){

        $pageExt = new PageExtension();

        $breadcrumb = new Breadcrumb();
        $breadcrumb->setSlug('test');
        $breadcrumb->setData('some data');

        $page = new Page();
        $page->setSlug('titlu')
            ->setTitle('Titlu')
            ->addBreadcrumb($breadcrumb);

        $this->assertEquals('some data',$pageExt->getPageBreadcrumb($page, 'test'));
        $this->assertEquals("Key 'inexistent' not found in page: titlu",$pageExt->getPageBreadcrumb($page, 'inexistent'));
    }
}