<?php
namespace Xaben\PageBundle\Twig\Extension;

class PageExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('page_breadcrumb', array($this, 'getPageBreadcrumb'), array('is_safe'=>array('html'))),
        );
    }

    public function getPageBreadcrumb($page, $key)
    {
        if (!is_object($page)) {
            return "Page not defined";
        }

        foreach ($page->getBreadcrumbs() as $breadcrumb) {
            if ($breadcrumb->getSlug() == $key) {
                return $breadcrumb->getData();
            }
        }

        return "Key '$key' not found in page: ".$page->getSlug();
    }

    public function getName()
    {
        return 'xaben_page_extension';
    }
}
